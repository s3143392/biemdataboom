package nl.utwente.di.bookQuote;

import java.awt.print.Book;

public class Quoter {
    public double getBookPrice(String book) {
        double price = 0;
        try{
            int isbn = Integer.parseInt(book);
            if (isbn == 1) {
                price = 10;
            } else if (isbn == 2) {
                price = 45;
            } else if (isbn == 3) {
                price = 20;
            } else if (isbn == 4) {
                price = 35;
            } else if (isbn == 5) {
                price = 50;
            }
        } catch (NumberFormatException e) {
            price = 0;
        }
        return price;
    }
}
